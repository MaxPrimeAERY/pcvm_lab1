import java.net.*;
import java.io.*;

public class MyServer {
    public static void main(String args[]) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(5221);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        while (true) {
            try {
                Socket socket = serverSocket.accept();

                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

                //сервер принял данные от клиента
                //String message = (String) ois.readObject();
                int brigadeNumber = (int) ois.readObject();
                String surname = null;
                if (brigadeNumber > 4 || brigadeNumber < 1){
                    surname = "Нету такой бригады";
                }
                switch (brigadeNumber) {
                    case 1:
                        surname = ("Сомов ");
                        break;
                    case 2:
                        surname = ("Добровський ");
                        break;
                    case 3:
                        surname = ("Крутелик ");
                        break;
                    case 4:
                        surname = ("Смит ");
                        break;
                }
                System.out.println("Номер запрошенной бригады: " + brigadeNumber);

                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                //сервер отправил ответ
                oos.writeObject(surname);

                ois.close();
                oos.close();
                socket.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}