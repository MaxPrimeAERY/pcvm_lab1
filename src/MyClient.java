import java.net.*;
import java.io.*;
import java.util.Scanner;

public class MyClient {
    public static void main(String[] args) {
        //get the localhost IP address, if server is running on some other IP, you need to use that

        Socket socket = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;

        try {
            socket = new Socket("127.0.0.1", 5221);
            oos = new ObjectOutputStream(socket.getOutputStream());
            Scanner input = new Scanner(System.in);
            System.out.print("Введите номер бригады = ");
            int brigade = input.nextInt();
            System.out.println("Отправка запроса серверу");

            //отправка серверу
            oos.writeObject(brigade);

            ois = new ObjectInputStream(socket.getInputStream());

            //ответ от сервера
            String message = (String) ois.readObject();
            System.out.println("Фамилия: " + message);

            ois.close();
            oos.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
